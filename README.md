**Desarrollo de aplicaciones web con conexion a base de datos**
**Semestre:**
*5AVP*
**Alumno:**
*Francisco Alonzo Lopez Murguia*

- Práctica #1 - 02/09/2022 - Práctica de Ejemplo
Commit: 847e9afeb66d73d73e5d649e8c2b6924b9df365e
Archivo: https://gitlab.com/franciscolopez22/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Práctica #2 - 09/09/2022 - Práctica JavaScript
Commit: 463a172ec53b6811b3585d35ce9e27ff2c90b176
Archivo: https://gitlab.com/franciscolopez22/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Práctica #3 - 15/09/2022 - Práctica web con bases de datos - parte 1 Commit: 651478b28cb19affe4b9a62856f7d98629769a51

- Práctica #4 - 19/09/2022 - Vista de consulta de base de datos Commit: 0f2dcdbb1f87fcb935d771fc66b82c8e06cc4066

- Práctica #5 - 22/09/2022 - Vista de registro de datos Commit: 9f43c8514578e9d132e49cdb2e479a5886c0cf0e

- Práctica #6 - 26/09/2022 - Conexion de base de datos Commit: 4d0731d84de48d9e5cdc04fe79903b9b6272f814
